require("@nomicfoundation/hardhat-toolbox");

/** @type import('hardhat/config').HardhatUserConfig */
module.exports = {
  solidity: {
    version: "0.8.9",
    settings: {
      optimizer: {
        enabled: true,
        runs: 200,
      },
    },
  },
  paths: {
    sources: "./ethereum/contracts",
    tests: "./test",
    cache: "./cache",
    artifacts: "./artifacts",
  },
  networks: {
    hardhat: {
      //Goerli fork
      forking: {
        url: "https://eth-goerli.g.alchemy.com/v2/qC4X-XqdyUq9z4jEnQXwa39QOPcrmAWj", //consider making this an env var
        blockNumber: 8000000,
      },
    },
    // mumbaiFork: {
    //   chainId: 1337,
    //   forking: {
    //     url: "https://polygon-mumbai.g.alchemy.com/v2/pkzUkwkRzeyQAPwdGwQkmmP50KCEKi4I", //consider making this an env var
    //     blockNumber: 28170000,
    //   },
    // },
    mumbai: {
      url: "https://polygon-mumbai.g.alchemy.com/v2/pkzUkwkRzeyQAPwdGwQkmmP50KCEKi4I", //process.env.ALCHEMY_MUMBAI_URL,
      accounts: [
        "13b3a53ef108f758ad9539ab8f3d20240d57ca49f513ea45203f9d6ea87bee72",
      ], //[process.env.ACCOUNT_KEY],
    },
    goerli: {
      url: "https://eth-goerli.g.alchemy.com/v2/qC4X-XqdyUq9z4jEnQXwa39QOPcrmAWj",
      accounts: [
        "430ac13c19646be5e9acfba1794677cab145ff40d53a937fc33402029305d7c4", //0x5833457ceCCe8395c094FCa83F231c28B83eb0C6
      ],
    },
    arbitrumGoerli: {
      url: "https://arb-goerli.g.alchemy.com/v2/kI02XVujfUazjAwb8A1kyjivSLnlH8me",
      accounts: [
        "430ac13c19646be5e9acfba1794677cab145ff40d53a937fc33402029305d7c4", //0x5833457ceCCe8395c094FCa83F231c28B83eb0C6
      ],
    },
    arbitrumOne: {
      url: "https://arb-mainnet.g.alchemy.com/v2/5vRZFz0n6KDpuVp1RfJ3bi0Ht5fGn3Lq",
      accounts: [
        "430ac13c19646be5e9acfba1794677cab145ff40d53a937fc33402029305d7c4", //0x5833457ceCCe8395c094FCa83F231c28B83eb0C6
      ],
    },
  },
};
