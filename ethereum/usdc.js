import web3 from "./web3";
import ERC20abi from "erc-20-abi";

export default (address) => {
  return new web3.eth.Contract(
    ERC20abi,
    //"0x07865c6E87B9F70255377e024ace6630C1Eaa37F" //USDC Address on Goerli
    "0x72a9c57cd5e2ff20450e409cf6a542f1e6c710fc" //USDC Address on Arbitrum Goerli
  );
};
