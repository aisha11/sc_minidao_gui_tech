//This file will be imported each time we need access to web3

//Using web3.js
// import Web3 from "web3";
// let web3;
// //web3 = new Web3('http://localhost:8545');

// if (typeof window !== "undefined" && typeof window.ethereum !== "undefined") {
//   // We are in the browser and metamask is running.
//   window.ethereum.request({ method: "eth_requestAccounts" });
//   web3 = new Web3(window.ethereum);
// } else {
//   // We are on the server *OR* the user is not running metamask
//   const provider = new Web3.providers.HttpProvider(
//     "https://eth-goerli.g.alchemy.com/v2/qC4X-XqdyUq9z4jEnQXwa39QOPcrmAWj" //process.env.ALCHEMY_MUMBAI_URL
//   );
//   web3 = new Web3(provider);
// }

//Using AlchemyWeb3
const { createAlchemyWeb3 } = require("@alch/alchemy-web3");

let web3;

if (typeof window !== "undefined" && typeof window.ethereum !== "undefined") {
  //If eth_accounts != 0 and we are in the desired chain then use web3(window.eth)
  //else use alchemy rpc

  // We are in the browser and metamask is running.
  window.ethereum.request({ method: "eth_requestAccounts" });
  //web3 = new Web3(window.ethereum);
  web3 = createAlchemyWeb3(
    "https://arb-mainnet.g.alchemy.com/v2/5vRZFz0n6KDpuVp1RfJ3bi0Ht5fGn3Lq"
    //"https://arb-goerli.g.alchemy.com/v2/kI02XVujfUazjAwb8A1kyjivSLnlH8me"
    //*{ writeProvider: window.ethereum }*//
  );
} else {
  // We are on the server *OR* the user is not running metamask

  //https://eth-goerli.g.alchemy.com/v2/l5oMSCcQFCjqtQG9ggTDEWHoooLYhTei
  web3 = createAlchemyWeb3(
    "https://arb-mainnet.g.alchemy.com/v2/5vRZFz0n6KDpuVp1RfJ3bi0Ht5fGn3Lq"
    //"https://arb-goerli.g.alchemy.com/v2/kI02XVujfUazjAwb8A1kyjivSLnlH8me"
  );
  console.log(web3);
}

// /**********************************************************/
// /* Handle chain (network) and chainChanged (per EIP-1193) */
// /**********************************************************/

// const chainId = await window.ethereum.request({ method: "eth_chainId" });
// // Do something with the chainId

// window.ethereum.on("chainChanged", handleChainChanged);

// function handleChainChanged(_chainId) {
//   // We recommend reloading the page, unless you must do otherwise
//   //if the chain does not match the chain we want
//   //then show the pop-up for user to connect to correct chain
//   //show toast as well
//   window.location.reload();
// }

// /***********************************************************/
// /* Handle user accounts and accountsChanged (per EIP-1193) */
// /***********************************************************/

// //This logic should be for the visibility of the "Connect wallet btn"

// let currentAccount = null;
// ethereum
//   .request({ method: "eth_accounts" })
//   .then(handleAccountsChanged)
//   .catch((err) => {
//     // Some unexpected error.
//     // For backwards compatibility reasons, if no accounts are available,
//     // eth_accounts will return an empty array.
//     console.error(err);
//   });

// // Note that this event is emitted on page load.
// // If the array of accounts is non-empty, you're already
// // connected.
// ethereum.on("accountsChanged", handleAccountsChanged);

// // For now, 'eth_accounts' will continue to always return an array
// function handleAccountsChanged(accounts) {
//   if (accounts.length === 0) {
//     // MetaMask is locked or the user has not connected any accounts
//     //Here were need to show the "Connect button"
//     console.log("Please connect to MetaMask.");
//   } else if (accounts[0] !== currentAccount) {
//     currentAccount = accounts[0];
//     //here we can show a toast message:
//     //"You are not connected to correct wallet account" if accounts[0] does not match database
//     // Do any other work!
//   }
// }

export default web3;
