const hre = require("hardhat");
const Web3 = require("web3");
const web3 = new Web3(hre.network.provider); // hre.network.provider is an EIP1193-compatible provider.
//const web3 = new Web3("https://arb1.arbitrum.io/rpc");
const compiledFactory = require("../artifacts/ethereum/contracts/Collateral.sol/CollateralFactory.json");

//Eth Goerli
// "0x07865c6E87B9F70255377e024ace6630C1Eaa37F",
// "0xD4a33860578De61DBAbDc8BFdb98FD742fA7028e"

//Arbitrum Goerli
// "0x72a9c57cd5e2ff20450e409cf6a542f1e6c710fc"
// "0x62CAe0FA2da220f43a51F86Db2EDb36DcA9A5A08"

//Arbitrum Mainnet
// "0xFF970A61A04b1cA14834A43f5dE4533eBDDB5CC8" //stablecoin
// "0x639Fe6ab55C921f74e7fac1ee960C0B6293ba612" //aggregator
async function main() {
  const accounts = await web3.eth.getAccounts();

  console.log("Attempting to deploy from accounts ", accounts[0]);

  let gasPrice = await web3.eth.getGasPrice();
  console.log(gasPrice);
  const factory = await new web3.eth.Contract(
    compiledFactory.abi
    //"0x3fdBA9f04e5e7d9F2F436F4184BB4897353f8922"
  )
    .deploy({ data: compiledFactory.bytecode })
    .send({ from: accounts[0], gasPrice: gasPrice }); //14000000

  //gas vs gasPrice vs gasLimit

  console.log("Contract deployed to", factory.options.address);
  //provider.engine.stop();

  // //4 users, 2 day cycle, 10$ contribution per cycle, 2 hour funding period, 60$ collateral
  // await factory.methods.createCollateral("4", "172800", "10", "7200", "60").send({
  //     from: accounts[0],
  //     gas: "10000000",
  // });
  //-----------------------------
  // await factory.methods
  //   .createCollateral(
  //     "4", //users
  //     "259200", //3 days
  //     "10",
  //     "172800", //2 days funding period
  //     "60",
  //     web3.utils.toWei("0.038", "ether"),
  //     "0xFF970A61A04b1cA14834A43f5dE4533eBDDB5CC8",
  //     "0x639Fe6ab55C921f74e7fac1ee960C0B6293ba612"
  //   )
  //   .send({
  //     from: accounts[0],
  //     gasPrice: gasPrice,
  //   });

  // [collateralAddress] = await factory.methods.getDeployedCollaterals().call();
  // console.log(collateralAddress);
  //------------------------------------
  // collateral = await new web3.eth.Contract(compiledCollateral.abi, collateralAddress);
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
