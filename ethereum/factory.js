import web3 from "./web3";
import CollateralFactory from "../artifacts/ethereum/contracts/Collateral.sol/CollateralFactory.json";

//This is a temp factory deployed on the testnet for frontend purposes
const instance = new web3.eth.Contract(
  CollateralFactory.abi,
  "0x55eB2D69eCEfd4487651285ca83bb8888830137a"
  //"0xAbA69f7e212042eE41c2320Fcd8B7754351D59cD"
  //"0x55eB2D69eCEfd4487651285ca83bb8888830137a" //first arbitrum factory
  //"0x8E8b13c7507FD2f91D4faa556EBE80f616667896"
  //"0x69FDAdbAAa7985F2AEd480e88f7e89127b1E369F"
  //"0xdE2A9c90bbf012fcCc197865b2eB911b0a04296b"
  //"0x80C6a82B4a09e7213de19D6CD5C629fB6819F149" //pretest 2 fail
  //"0x9A3d143071e4140970Fa26B63bCB5648C6Aa0BcE"
  //"0xcec42B9D1247dCb1ed84f1C64bB53DecDb53bC0c"
  //"0x64FE6BC092AE462C95511d76EB4Ec46a272e0E0B"
  //"0x6cF8348EfaCFc92d077516D19512c7c2d203E184"
  //'0xe5dEfbb5dC489FbFc0A4fed10c59C74e1a268210' //replace with var
  //address of deployed factory
);

export default instance;
