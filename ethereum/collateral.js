import web3 from "./web3";
import Collateral from "../artifacts/ethereum/contracts/Collateral.sol/Collateral.json";

export default (address) => {
  return new web3.eth.Contract(Collateral.abi, address);
};
