import web3 from "./web3";
import Fund from "../artifacts/ethereum/contracts/Fund.sol/Fund.json";

export default (address) => {
  return new web3.eth.Contract(Fund.abi, address);
};
