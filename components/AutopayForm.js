import React, { Component } from "react";
import { Form, Button, Input, Message } from "semantic-ui-react";
import Fund from "../ethereum/fund";
import web3 from "../ethereum/web3";
import Router, { withRouter } from "next/router";

class AutopayForm extends Component {
  state = {
    errorMessage: "",
    loading: false,
    successMessage: "",
    addressBehalf: "",
    active: this.props.autoPayState,
  };

  onSubmit = async (event) => {
    event.preventDefault();

    this.setState({ loading: true, errorMessage: "", successMessage: "" });
    const fund = Fund(this.props.address); //Step 1: create instance of SC
    console.log("Autupay state is:", fund);
    try {
      const accounts = await web3.eth.getAccounts();
      console.log(accounts);
      await fund.methods.toggleAutoPay().send({
        from: accounts[0],
      });

      this.setState((prevState) => ({ active: !prevState.active }));

      Router.reload();
      //Router.replaceRoute(`/collaterals/${this.props.address}`);
      this.setState({ successMessage: "Transaction Successful!" });
    } catch (err) {
      this.setState({ errorMessage: err.message });
    }

    this.setState({ loading: false });
  };
  render() {
    const { active } = this.state;

    return (
      <Form
        onSubmit={this.onSubmit}
        error={!!this.state.errorMessage}
        success={!!this.state.successMessage}
      >
        <Message error header="Oops" content={this.state.errorMessage} />
        <Message success header="Yay!" content={this.state.successMessage} />
        <Button toggle active={active} loading={this.state.loading}>
          Toggle me to enable auto pay!
        </Button>
      </Form>
    );
  }
}

export default AutopayForm;
