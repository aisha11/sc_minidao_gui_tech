import React, { Component } from "react";
import { Form, Button, Input, Message } from "semantic-ui-react";
import Collateral from "../ethereum/collateral";
import Fund from "../ethereum/fund";
import web3 from "../ethereum/web3";
import Router, { withRouter } from "next/router";

class WithdrawForm extends Component {
  state = {
    errorMessage: "",
    loading: false,
    successMessage: "",
  };

  onSubmit = async (event) => {
    event.preventDefault();

    this.setState({ loading: true, errorMessage: "", successMessage: "" });

    if (this.props.withdrawType == "collateral") {
      const collateral = Collateral(this.props.address);

      try {
        const accounts = await web3.eth.getAccounts();
        await collateral.methods.withdrawCollateral().send({
          from: accounts[0],
        });

        const amount = await collateral.methods
          .collateralPaymentBank(accounts[0])
          .call();
        console.log("reimbursment is: ", amount);
        if (amount > 0) {
          await collateral.methods.withdrawReimbursement().send({
            from: accounts[0],
          });
        }

        Router.reload();
        //Router.replaceRoute(`/collaterals/${this.props.address}`);
        this.setState({ successMessage: "Transaction Successful!" });
      } catch (err) {
        this.setState({ errorMessage: err.message });
      }
    } else {
      const fund = Fund(this.props.address);

      try {
        const accounts = await web3.eth.getAccounts();
        await fund.methods.withdrawFund().send({
          from: accounts[0],
        });

        Router.reload();
        //Router.replaceRoute(`/collaterals/${this.props.address}`);
        this.setState({ successMessage: "Transaction Successful!" });
      } catch (err) {
        this.setState({ errorMessage: err.message });
      }
    }

    this.setState({ loading: false });
  };

  render() {
    return (
      <Form
        onSubmit={this.onSubmit}
        error={!!this.state.errorMessage}
        success={!!this.state.successMessage}
      >
        <Button primary loading={this.state.loading}>
          {this.props.name}
        </Button>
        <Message error header="Oops" content={this.state.errorMessage} />
        <Message success header="Yay!" content={this.state.successMessage} />
      </Form>
    );
  }
}

export default WithdrawForm;

// const accounts = await web3.eth.getAccounts();
// await collateral.methods.withdrawCollateral().send({
//   from: accounts[0],
// });
