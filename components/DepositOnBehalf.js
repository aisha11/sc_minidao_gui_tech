import React, { Component } from "react";
import { Form, Button, Input, Message } from "semantic-ui-react";
import Fund from "../ethereum/fund";
import web3 from "../ethereum/web3";
import Router, { withRouter } from "next/router";

class DepositOnBehalfForm extends Component {
  state = {
    errorMessage: "",
    loading: false,
    successMessage: "",
    addressBehalf: "",
  };

  onSubmit = async (event) => {
    event.preventDefault();

    this.setState({ loading: true, errorMessage: "", successMessage: "" });

    console.log("Pay on behalf of this address: ", this.state.addressBehalf);
    const fund = Fund(this.props.address); //Step 1: create instance of SC

    try {
      const accounts = await web3.eth.getAccounts();
      await fund.methods
        .payContributionOnBehalfOf(this.state.addressBehalf)
        .send({
          from: accounts[0],
        });

      Router.reload();
      //Router.replaceRoute(`/collaterals/${this.props.address}`);
      this.setState({ successMessage: "Transaction Successful!" });
    } catch (err) {
      this.setState({ errorMessage: err.message });
    }

    this.setState({ loading: false });
  };
  render() {
    return (
      <Form
        onSubmit={this.onSubmit}
        error={!!this.state.errorMessage}
        success={!!this.state.successMessage}
      >
        <Form.Field>
          <Input
            label="address"
            labelPosition="right"
            placeholder="ex. 0x1243abcd..."
            value={this.state.addressBehalf}
            onChange={(event) =>
              this.setState({ addressBehalf: event.target.value })
            }
          />
        </Form.Field>
        <Message error header="Oops" content={this.state.errorMessage} />
        <Message success header="Yay!" content={this.state.successMessage} />
        <Button loading={this.state.loading} primary>
          Pay On Behalf
        </Button>
      </Form>
    );
  }
}

export default DepositOnBehalfForm;
