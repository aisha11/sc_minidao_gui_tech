import React from "react";
import { Menu } from "semantic-ui-react";
import Link from "next/link";

const Header = (props) => {
  return (
    <Menu style={{ marginTop: "10px" }}>
      <Link href="/">
        <Menu.Item>Takaturn</Menu.Item>
      </Link>

      <Menu.Menu position="right">
        <Link href="/">
          <Menu.Item>Collateral</Menu.Item>
        </Link>

        <Link href="/collaterals/new/">
          <Menu.Item>+</Menu.Item>
        </Link>
      </Menu.Menu>
    </Menu>
  );
};

export default Header;
