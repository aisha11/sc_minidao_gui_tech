import React, { Component } from "react";
import { Form, Button, Input, Message } from "semantic-ui-react";
import Collateral from "../ethereum/collateral";
import Fund from "../ethereum/fund";
import web3 from "../ethereum/web3";
import Router, { withRouter } from "next/router";
import Usdc from "../ethereum/usdc";

class DepositForm extends Component {
  state = {
    errorMessage: "",
    loading: false,
    successMessage: "",
  };

  onSubmit = async (event) => {
    event.preventDefault();

    this.setState({ loading: true, errorMessage: "", successMessage: "" });

    if (this.props.depositType == "collateral") {
      console.log("amount is", this.props.amount);
      const collateral = Collateral(this.props.address); //Step 1: create instance of SC
      const amount = this.props.amount;

      try {
        const accounts = await web3.eth.getAccounts();
        console.log("getting the accounts worked", accounts[0]);
        await collateral.methods.depositCollateral().send({
          from: accounts[0],
          value: amount, //web3.utils.toWei(collateralAmount.toString(), "ether"),
        });

        //Router.reload();
        //Router.replaceRoute(`/collaterals/${this.props.address}`);
        this.setState({ successMessage: "Transaction Successful!" });
      } catch (err) {
        this.setState({ errorMessage: err.message });
      }
    } else {
      const fund = Fund(this.props.address);
      const usdc = Usdc(); //Step 1: create instance of SC

      const currentCycle = await fund.methods.currentCycle().call();
      console.log("Current cycle is", currentCycle);
      const totalParticipants = await fund.methods.totalParticipants().call();
      console.log("Current cycle is", totalParticipants);

      if (currentCycle == 1) {
        //only approve first time
        try {
          const accounts = await web3.eth.getAccounts();
          const amount = totalParticipants * this.props.amount;
          console.log("amount is ", this.props.amount);
          await usdc.methods.approve(this.props.address, amount).send({
            from: accounts[0],
          }); //request approval from USDC first
        } catch (err) {
          this.setState({ errorMessage: err.message });
        }
      }

      console.log("Trying to pay contribution");
      try {
        const accounts = await web3.eth.getAccounts();
        const transaction = await fund.methods.payContribution().send({
          from: accounts[0],
        });

        Router.reload();
        //Router.replaceRoute(`/collaterals/${this.props.address}`);
        console.log("Transaction is: ", transaction);
        this.setState({ successMessage: "Transaction Successful!" });
      } catch (err) {
        this.setState({ errorMessage: err.message });
      }
    }

    this.setState({ loading: false });
  };
  render() {
    return (
      <Form
        onSubmit={this.onSubmit}
        error={!!this.state.errorMessage}
        success={!!this.state.successMessage}
      >
        <Button primary loading={this.state.loading}>
          {this.props.name}
        </Button>
        <Message error header="Oops" content={this.state.errorMessage} />
        <Message success header="Yay!" content={this.state.successMessage} />
      </Form>
    );
  }
}

export default DepositForm;
