import React, { Component } from "react";
import { Card, Button } from "semantic-ui-react";
import Layout from "../components/Layout";
import factory from "../ethereum/factory";
import Link from "next/link";

class CollateralIndex extends Component {
  static async getInitialProps() {
    //this is a next lifecycle method, to factor back to react, use componentDidMount

    console.log(factory.options.address);
    const collaterals = await factory.methods.getDeployedCollaterals().call();
    console.log(collaterals);
    return { collaterals };
  }

  renderCollaterals() {
    const items = this.props.collaterals.map((address) => {
      return {
        header: address,
        description: (
          <Link href={`/collaterals/${address}`}>
            {/* collateral/[show]/index.js */}
            <a>View Collateral</a>
          </Link>
        ),
        fluid: true,
      };
    });

    return <Card.Group items={items} />;
  }

  render() {
    return (
      <Layout>
        <div>
          <h3>Open Collaterals</h3>

          <Link href="/collaterals/new/">
            <Button
              floated="right"
              content="Create Collateral"
              icon="add circle"
              primary
            />
          </Link>
          {this.renderCollaterals()}
        </div>
      </Layout>
    );
  }
}

export default CollateralIndex;
