import React, { Component } from "react";
import { Button, Card, Grid } from "semantic-ui-react";
import Layout from "../../../components/Layout";
import Collateral from "../../../ethereum/collateral";
import web3 from "../../../ethereum/web3";
import DepositForm from "../../../components/DepositeForm";
import WithdrawForm from "../../../components/WithdrawForm";
import Link from "next/link";

class CollateralShow extends Component {
  static async getInitialProps(props) {
    const collateral = Collateral(props.query.show);
    const summary = await collateral.methods.getSummary().call();
    const fundAddress = await collateral.methods.fundContract().call();

    // let stages =[
    //   Stages.AcceptingCollateral
    // ];

    return {
      address: props.query.show,
      fundAddress: fundAddress,
      currentStage: summary[0],
      cycleTime: summary[1],
      totalParticipants: summary[2],
      requiredCollateralDeposit: summary[3],
      requiredContribution: summary[4],
      contributionPeriod: summary[5],
      currentMemberCount: summary[6],
      fixedCollateralEth: summary[7],
    };
  }

  renderCards() {
    const {
      currentStage,
      cycleTime,
      totalParticipants,
      requiredCollateralDeposit,
      requiredContribution,
      contributionPeriod,
      currentMemberCount,
    } = this.props;

    const Stages = {
      0: "Deposit Your Collateral",
      1: "Cycle is Ongoing",
      2: "Withdraw Your Collateral",
      3: "Closed Contract",
    };

    const items = [
      {
        header: Stages[currentStage],
        meta: "Current Collateral Stage",
        description:
          "The current stage tells you when you can deposit and withdraw your collateral money",
        style: { overflowWrap: "break-word" },
      },
      {
        header: cycleTime / 86400, //convert to days
        meta: "Cycle Duration (days)",
        description:
          "This is the time of 1 cycle in the term. A beneficiary is selected at the end of every cycle",
        style: { overflowWrap: "break-word" },
      },
      {
        header: totalParticipants,
        meta: "Total Participants",
        description:
          "The total number of participants also indicate the total number of cycles in a term",
        style: { overflowWrap: "break-word" },
      },
      {
        header: requiredCollateralDeposit / 10 ** 18,
        meta: "Required Deposit (USD)",
        description:
          "Each user must deposit this amount to enter into the fund.",
        style: { overflowWrap: "break-word" },
      },
      {
        header: requiredContribution,
        meta: "Required Contribution (USD)",
        description: "This is the amount you need to contribution per cycle.",
        style: { overflowWrap: "break-word" },
      },
      {
        header: contributionPeriod / 3600, //convert to hours
        meta: "Contribution Period (hours)",
        description:
          "The time window that is open for contributions at the start of each cycle.",
        style: { overflowWrap: "break-word" },
      },
      {
        header: currentMemberCount,
        meta: "Current Member Count",
        description:
          "The current number of users enrolled in this term. The members must match the total participant to start a cycle.",
        style: { overflowWrap: "break-word" },
      },
    ];

    return <Card.Group items={items} />;
  }

  render() {
    return (
      <Layout>
        <h3> Term Information </h3>
        <Grid>
          <Grid.Row>
            <Grid.Column width={14}>{this.renderCards()}</Grid.Column>

            <Grid.Column width={2}>
              <DepositForm
                address={this.props.address}
                depositType="collateral"
                amount={this.props.fixedCollateralEth}
                name="Deposit Collateral"
              />
              <hr />
              <WithdrawForm
                address={this.props.address}
                withdrawType="collateral"
                name="Withdraw Collateral"
              />
            </Grid.Column>
          </Grid.Row>

          <Grid.Row>
            <Grid.Column>
              <Link
                href={`/collaterals/${this.props.address}/${this.props.fundAddress}`}
              >
                <Button primary disabled={!(this.props.fundAddress != 0)}>
                  View Cycle Information
                </Button>
              </Link>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Layout>
    );
  }
}

export default CollateralShow;

// stage, //current state of Collateral
//   cycleTime, //cycle duration
//   totalParticipants, //total no. of participants
//   collateralDeposit, //collateral
//   contributionAmount, //Required contribution per cycle
//   contributionPeriod, //time to contribute
//   counterMembers; //current member count
