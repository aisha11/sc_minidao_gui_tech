import React, { Component } from "react";
import { Button, Table, Grid, Card } from "semantic-ui-react";
import Layout from "../../../../components/Layout";
import web3 from "../../../../ethereum/web3";
import Fund from "../../../../ethereum/fund";
import Collateral from "../../../../ethereum/collateral";
import DepositForm from "../../../../components/DepositeForm";
import WithdrawForm from "../../../../components/WithdrawForm";
import AutopayForm from "../../../../components/AutopayForm";
import UserRow from "../../../../components/UserRow";
import Link from "next/link";
import Head from "next/head";

// const FundStates = {
//   InitializingFund: "0",
//   AcceptingContributions: "1", // collect the funds
//   ChoosingBeneficiary: "2",
//   CycleOngoing: "3",
//   FundClosed: "4", // When active cycle compelted
// };

const FundStates = {
  0: "InitializingFund",
  1: "AcceptingContributions", // collect the funds
  2: "ChoosingBeneficiary",
  3: "CycleOngoing",
  4: "FundClosed", // When active cycle compelted
};

class FundIndex extends Component {
  static async getInitialProps(props) {
    console.log("Fund address is", props.query.funds);
    const fund = Fund(props.query.funds);

    const collateralAddress = await fund.methods.collateral().call();
    console.log("Collateral address is:", collateralAddress);
    const collateral = Collateral(collateralAddress);

    const currentCycle = await fund.methods.currentCycle().call();
    const currentState = await fund.methods.currentState().call();

    const requiredContribution = await fund.methods.contributionAmount().call();

    const participantCount = await collateral.methods.counterMembers().call();
    const participantsAddresses = await Promise.all(
      Array(parseInt(participantCount))
        .fill()
        .map((element, index) => {
          return collateral.methods.participants(index).call();
        })
    );

    const beneficiariesTracker = await Promise.all(
      Array(parseInt(participantCount))
        .fill()
        .map((element, index) => {
          return fund.methods
            .beneficiariesTracker(participantsAddresses[index])
            .call();
        })
    );

    const defaultersTracker = false;
    // await Promise.all(
    //   //defaulter?
    //   Array(parseInt(participantCount))
    //     .fill()
    //     .map((element, index) => {
    //       return fund.methods
    //         .defaultersTracker(participantsAddresses[index])
    //         .call() ;
    //     })
    // );

    const participantsBanks = await Promise.all(
      Array(parseInt(participantCount))
        .fill()
        .map((element, index) => {
          return fund.methods
            .beneficiariesPool(participantsAddresses[index])
            .call();
        })
    );

    const paidThisCycle = await Promise.all(
      //defaulter?
      Array(parseInt(participantCount))
        .fill()
        .map((element, index) => {
          return fund.methods
            .paidThisCycle(participantsAddresses[index])
            .call();
        })
    );

    const collateralMembersBank = await Promise.all(
      Array(parseInt(participantCount))
        .fill()
        .map((element, index) => {
          return collateral.methods
            .collateralMembersBank(participantsAddresses[index])
            .call();
        })
    );

    const collateralPaymentBank = await Promise.all(
      Array(parseInt(participantCount))
        .fill()
        .map((element, index) => {
          return collateral.methods
            .collateralPaymentBank(participantsAddresses[index])
            .call();
        })
    );

    const beneficiariesOrder = await Promise.all(
      Array(parseInt(participantCount))
        .fill()
        .map((element, index) => {
          return fund.methods.beneficiariesOrder(index).call();
        })
    );

    const expelledTracker = await Promise.all(
      Array(parseInt(participantCount))
        .fill()
        .map((element, index) => {
          return !fund.methods
            .participantsTracker(participantsAddresses[index])
            .call();
        })
    );

    const autopayTracker = await Promise.all(
      Array(parseInt(participantCount))
        .fill()
        .map((element, index) => {
          return fund.methods
            .autoPayEnabled(participantsAddresses[index])
            .call();
        })
    );

    //this may fail if user is not connected to wallet
    let autoPayState = false;
    try {
      const accounts = await web3.eth.getAccounts();
      autoPayState = await fund.methods.autoPayEnabled(accounts[0]).call();
    } catch (err) {
      console.log(err);
    }

    console.log(participantsAddresses);
    console.log(beneficiariesTracker);
    console.log(defaultersTracker);
    console.log(participantsBanks);
    console.log(requiredContribution);
    console.log("Collateral member banks: ", collateralMembersBank);
    console.log("Collateral payment banks: ", collateralPaymentBank);
    console.log(beneficiariesOrder);

    // let stages =[
    //   Stages.AcceptingCollateral
    // ];

    return {
      address: props.query.funds,
      participantsAddresses,
      beneficiariesTracker,
      defaultersTracker,
      participantsBanks,
      collateralMembersBank,
      collateralPaymentBank,
      paidThisCycle,
      requiredContribution,
      beneficiariesOrder,
      currentCycle,
      expelledTracker,
      currentState,
      autoPayState,
      autopayTracker,
    };
  }

  renderCards() {
    const { currentCycle, currentState } = this.props;

    const items = [
      {
        header: currentCycle, //convert to days
        meta: "Current Cycle",
        description: "Withdraw your contribution if this matches your cycle.",
        style: { overflowWrap: "break-word" },
      },
      {
        header: FundStates[currentState], //convert to days
        meta: "Current State",
        description: "This is the current status of the fund.",
        style: { overflowWrap: "break-word" },
      },
    ];

    return <Card.Group items={items} />;
  }

  renderRows() {
    const { Row, Cell } = Table;
    const addressMapping = new Map([
      // ["0x98D522Eef12A7B474B28aF15352017b7A1B42054", "Leila"],
      // ["0x3DE0494c6DfbC2e3C58F5c8a2b9CEA34bE24F161", "Amit"],
      // ["0x116E7F4aa439348Aec838f4bc6cfD77cCD4Ea245", "Vishal"],
      // ["0xa42C8974E43a74De3c70b8246CC0394bb2309401", "Mohammed"],
      // ["0x92aE5285Ed66cF37B4A7A6F5DD345E2b11be90fd", "Sharene"],
      // ["0xD81140D6A98802CCB8b3b59626Ea7B895625BeBa", "Ateeq"],
      // ["0xb059Fc3f7111A3FA32f5291AC80f7dc5299B8a16", "Abdalla"],
      // ["0x2961013Ad58dC04A4C81122aDEB01B0F9c75192D", "Akram"],
      ["0xFc3ef6B852F13593035667253F09a8505f2a899e", "Roaa"],
      ["0x4934722B65FEc2c8F07973BD48a2a8E1A982094D", "Maryam"],
      ["0xD77Fb0a04294fa9886F9937D42811Dfcaa15652E", "Ameerah"],
      ["0x25CeFdC8F6c12285B2835F39b9c7D5171CAeB2bB", "Santosh"],
    ]);

    return this.props.participantsAddresses.map((address, index) => {
      return (
        <Row key={index}>
          <Cell>
            {addressMapping.get(address)
              ? addressMapping.get(address)
              : address}
          </Cell>

          <Cell>{this.props.beneficiariesTracker[index].toString()}</Cell>
          <Cell>{this.props.participantsBanks[index]}</Cell>
          <Cell>{this.props.paidThisCycle[index].toString()}</Cell>
          <Cell>{this.props.expelledTracker[index].toString()}</Cell>
          <Cell>{this.props.autopayTracker[index].toString()}</Cell>
          <Cell>
            {web3.utils.fromWei(
              (
                parseInt(this.props.collateralMembersBank[index]) +
                parseInt(this.props.collateralPaymentBank[index])
              ).toString()
            )}
          </Cell>
          <Cell>
            {"Cycle " + (this.props.beneficiariesOrder.indexOf(address) + 1)}
          </Cell>
        </Row>
      );
    });
  }

  render() {
    const { Header, Row, HeaderCell, Body } = Table;
    return (
      <Layout>
        <h3> Fund Information </h3>
        <Grid verticalAlign="middle" columns="four">
          <Grid.Row>
            <Grid.Column>{this.renderCards()}</Grid.Column>

            <Grid.Column>
              <DepositForm
                address={this.props.address}
                depositType="fund"
                amount={this.props.requiredContribution}
                name="Deposit Contribution"
              />
            </Grid.Column>
            <Grid.Column>
              <WithdrawForm
                address={this.props.address}
                withdrawType="fund"
                name="Withdraw Contribution"
              />
            </Grid.Column>
            <Grid.Column>
              <AutopayForm
                address={this.props.address}
                autoPayState={this.props.autoPayState}
              />
            </Grid.Column>
          </Grid.Row>
        </Grid>

        <Table>
          <Header>
            <Row>
              <HeaderCell>Address</HeaderCell>

              <HeaderCell>Was a Beneficiary?</HeaderCell>
              <HeaderCell>Contribution Bank</HeaderCell>
              <HeaderCell>Paid This Cycle?</HeaderCell>
              <HeaderCell>Expelled?</HeaderCell>
              <HeaderCell>Autopay?</HeaderCell>
              <HeaderCell>Collateral Bank</HeaderCell>
              <HeaderCell>Beneficiary Order</HeaderCell>
            </Row>
          </Header>
          <Body>{this.renderRows()}</Body>
        </Table>
      </Layout>
    );
  }
}

export default FundIndex;
