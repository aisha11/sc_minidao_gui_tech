import React, { Component } from "react";
import { Form, Button, Input, Message } from "semantic-ui-react";
import Layout from "../../components/Layout";
import factory from "../../ethereum/factory";
import web3 from "../../ethereum/web3";
import Router, { withRouter } from "next/router";

class CollateralNew extends Component {
  state = {
    totalParticipants: "",
    contributionAmount: "",
    cycleTime: "",
    contributionPeriod: "",
    errorMessage: "",
    loading: false,
  };

  onSubmit = async (event) => {
    event.preventDefault();

    this.setState({ loading: true, errorMessage: "" });

    //  function createCollateral(uint totalParticipants, uint cycleTime, uint contributionAmount, uint contributionPeriod, uint collateralAmount) public {
    const cycleTimeSeconds = this.state.cycleTime * 24 * 60 * 60;
    const contributionPereiodSeconds = this.state.contributionPeriod * 60 * 60;
    const collateralAmount =
      this.state.totalParticipants * this.state.contributionAmount * 1.5;
    const fixedCollateralEth = collateralAmount * 0.0006; //fixed exchange rate
    try {
      const accounts = await web3.eth.getAccounts();
      await factory.methods
        .createCollateral(
          this.state.totalParticipants,
          cycleTimeSeconds,
          this.state.contributionAmount,
          contributionPereiodSeconds,
          collateralAmount,
          web3.utils.toWei(fixedCollateralEth.toString(), "ether"),
          "0x72a9c57cd5e2ff20450e409cf6a542f1e6c710fc", //arbitrum
          "0x62CAe0FA2da220f43a51F86Db2EDb36DcA9A5A08"
        )
        .send({
          from: accounts[0],
        });

      Router.push("/");
    } catch (err) {
      this.setState({ errorMessage: err.message });
    }

    this.setState({ loading: false });
  };

  render() {
    return (
      <Layout>
        <h1>Create a New Collateral</h1>

        <Form onSubmit={this.onSubmit} error={!!this.state.errorMessage}>
          <Form.Field>
            <label> Total Participants </label>
            <Input
              label="#"
              labelPosition="right"
              placeholder="ex. 4"
              value={this.state.totalParticipants}
              onChange={(event) =>
                this.setState({ totalParticipants: event.target.value })
              }
            />
          </Form.Field>
          <Form.Field>
            <label> Contribution Amount </label>
            <Input
              label="$"
              labelPosition="right"
              placeholder="ex. 100"
              value={this.state.contributionAmount}
              onChange={(event) =>
                this.setState({ contributionAmount: event.target.value })
              }
            />
          </Form.Field>
          <Form.Field>
            <label> Cycle Time </label>
            <Input
              label="days"
              labelPosition="right"
              placeholder="ex. 30"
              value={this.state.cycleTime}
              onChange={(event) =>
                this.setState({ cycleTime: event.target.value })
              }
            />
          </Form.Field>
          <Form.Field>
            <label> Contribution Period Duration </label>
            <Input
              label="hours"
              labelPosition="right"
              placeholder="ex. 24"
              value={this.state.contributionPeriod}
              onChange={(event) =>
                this.setState({ contributionPeriod: event.target.value })
              }
            />
          </Form.Field>

          <Message error header="Oops" content={this.state.errorMessage} />
          <Button loading={this.state.loading} primary>
            Create!
          </Button>
        </Form>
      </Layout>
    );
  }
}

export default CollateralNew;
//uint totalParticipants, uint cycleTime, uint contributionAmount, uint contributionPeriod, uint collateralAmount)
